from model import AmbarNamedEntity, ExternalNER
from apiproxy import ApiProxy
from logger import AmbarLogger
import re

class NERecognizer:
    def __init__(self, Logger, ApiProxy):
        self.logger = Logger
        self.apiProxy = ApiProxy 

    def RecognizeAndAddNEsToAmbarFile(self, IndexName, AmbarFile):
        namedEntities = []
        self.RecognizeEmails(IndexName, AmbarFile, namedEntities)
        self.RecognizePhones(IndexName, AmbarFile, namedEntities)
        self.RecognizeVehicleRegistrationIds(IndexName, AmbarFile, namedEntities)
        self.RecognizeTaxIds(IndexName, AmbarFile, namedEntities)
        self.RecognizeCompanyNames(IndexName, AmbarFile, namedEntities)
        self.RecognizeIpV4s(IndexName, AmbarFile, namedEntities)
        self.RecognizeIpV6s(IndexName, AmbarFile, namedEntities)
        self.RecognizeUris(IndexName, AmbarFile, namedEntities)
        if (len(namedEntities) > 0):
            self.AddNamedEntitiesToAmbarFile(IndexName, AmbarFile['file_id'], AmbarFile['meta']['full_name'], namedEntities)
        return namedEntities
    
    def AddNamedEntitiesToAmbarFile(self, IndexName, FileId, FullName, NamedEntities):
        apiResp = self.apiProxy.AddFileNamedEntities(IndexName, FileId, NamedEntities)

        if not apiResp.Success: 
            self.logger.LogMessage('error', 'error adding {0} named entities to file {1} {2}'.format(len(NamedEntities), FullName, apiResp.message))
            return False
        
        if not (apiResp.Ok or apiResp.Created):
            self.logger.LogMessage('error', 'error adding {0} named entities to file, unexpected response code {1} {2} {3}'.format(len(NamedEntities), FullName, apiResp.code, apiResp.message))
            return False
        
        self.logger.LogMessage('verbose', '{0} named entities added to {1}'.format(len(NamedEntities), FullName))

    def RecognizeEmails(self, IndexName, AmbarFile, NamedEntities):
        emailRegex = re.compile('(^|\W)([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)($|\W)', re.M)
        for match in emailRegex.finditer(AmbarFile['content']['text']):
            email = match.group(2).lower()
            position = match.span(2)
            if email[-1:] == '.':
                email = email[:-1]
                position = [ position[0], position[1] - 1 ]
            emailEntity = AmbarNamedEntity.Init(email, 'email', position[0], position[1] - position[0])
            NamedEntities.append(emailEntity)

    def RecognizePhones(self, IndexName, AmbarFile, NamedEntities):
        phoneRegex = re.compile('(\+|\W)((7|8)((?:[\t\s+\-()]*[0-9]){10}))(\D|$)', re.M)
        for match in phoneRegex.finditer(AmbarFile['content']['text']):
            phone = match.group(4).lower()
            phone = re.sub('[^0-9]','','7{0}'.format(phone))
            position = match.span(2)
            phoneEntity = AmbarNamedEntity.Init(phone, 'phone', position[0], position[1] - position[0])
            NamedEntities.append(phoneEntity)
    
    def RecognizeVehicleRegistrationIds(self, IndexName, AmbarFile, NamedEntities):
        vehicleRegistrationIdRegex = re.compile('(^|\W)([АВЕКМНОРСТУХABEKMHOPCTYX]\d{3}[АВЕКМНОРСТУХABEKMHOPCTYX]{2}\d{2,3})($|\W)', re.M | re.I)
        for match in vehicleRegistrationIdRegex.finditer(AmbarFile['content']['text']):
            regId = match.group(2).upper()
            regId =  regId.replace('A','А').replace('B','В').replace('E','Е').replace('K','К').replace('M','М').replace('H','Н').replace('O','О').replace('P','Р').replace('C','С').replace('T','Т').replace('Y','У').replace('X','Х')
            position = match.span(2)
            regIdEntity = AmbarNamedEntity.Init(regId, 'veh-reg-id', position[0], position[1] - position[0])
            NamedEntities.append(regIdEntity)

    def RecognizeTaxIds(self, IndexName, AmbarFile, NamedEntities):
        taxIdRegex = re.compile('(^|\W)([0-9]{10,12})($|\W)', re.M | re.I)
        for match in taxIdRegex.finditer(AmbarFile['content']['text']):
            inn = match.group(2)
            position = match.span(2)
            if (self.CheckINN(inn)):
                taxIdEntity = AmbarNamedEntity.Init(inn, 'tax-id', position[0], position[1] - position[0])
                NamedEntities.append(taxIdEntity)

    def RecognizeCompanyNames(self, IndexName, AmbarFile, NamedEntities):
        companyNameRegex = re.compile('(\W|^)(((ООО)|(ОАО)|(АО)|(ПАО)|(ЗАО))([\W]*)(\")([^\"]+)(\"))', re.M | re.I)
        for match in companyNameRegex.finditer(AmbarFile['content']['text']):
            companyName = match.group(2).lower()
            companyName = re.sub('[^a-zа-яёй0-9\-\s]','',companyName)
            companyName = re.sub('[\s]+',' ',companyName)
            companyName = companyName.strip()
            position = match.span(2)
            companyNameEntity = AmbarNamedEntity.Init(companyName, 'company-name', position[0], position[1] - position[0])
            NamedEntities.append(companyNameEntity)

    def RecognizeIpV4s(self, IndexName, AmbarFile, NamedEntities):
        ipV4Regex = re.compile('(^|\W)(((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))($|\W)', re.M)
        for match in ipV4Regex.finditer(AmbarFile['content']['text']):
            ipV4 = match.group(2)
            position = match.span(2)
            ipV4Entity = AmbarNamedEntity.Init(ipV4, 'ip', position[0], position[1] - position[0])
            NamedEntities.append(ipV4Entity)
    
    def RecognizeIpV6s(self, IndexName, AmbarFile, NamedEntities):
        ipV6Regex = re.compile('(^|\W)((?:(?:[0-9A-Fa-f]{1,4}:){6}(?:[0-9A-Fa-f]{1,4}:[0-9A-Fa-f]{1,4}|(?:(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))|::(?:[0-9A-Fa-f]{1,4}:){5}(?:[0-9A-Fa-f]{1,4}:[0-9A-Fa-f]{1,4}|(?:(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))|(?:[0-9A-Fa-f]{1,4})?::(?:[0-9A-Fa-f]{1,4}:){4}(?:[0-9A-Fa-f]{1,4}:[0-9A-Fa-f]{1,4}|(?:(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))|(?:[0-9A-Fa-f]{1,4}:[0-9A-Fa-f]{1,4})?::(?:[0-9A-Fa-f]{1,4}:){3}(?:[0-9A-Fa-f]{1,4}:[0-9A-Fa-f]{1,4}|(?:(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))|(?:(?:[0-9A-Fa-f]{1,4}:){,2}[0-9A-Fa-f]{1,4})?::(?:[0-9A-Fa-f]{1,4}:){2}(?:[0-9A-Fa-f]{1,4}:[0-9A-Fa-f]{1,4}|(?:(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))|(?:(?:[0-9A-Fa-f]{1,4}:){,3}[0-9A-Fa-f]{1,4})?::[0-9A-Fa-f]{1,4}:(?:[0-9A-Fa-f]{1,4}:[0-9A-Fa-f]{1,4}|(?:(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))|(?:(?:[0-9A-Fa-f]{1,4}:){,4}[0-9A-Fa-f]{1,4})?::(?:[0-9A-Fa-f]{1,4}:[0-9A-Fa-f]{1,4}|(?:(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}(?:[0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))|(?:(?:[0-9A-Fa-f]{1,4}:){,5}[0-9A-Fa-f]{1,4})?::[0-9A-Fa-f]{1,4}|(?:(?:[0-9A-Fa-f]{1,4}:){,6}[0-9A-Fa-f]{1,4})?::))($|\W)', re.M)
        for match in ipV6Regex.finditer(AmbarFile['content']['text']):
            ipV6 = match.group(2)
            position = match.span(2)
            ipV6Entity = AmbarNamedEntity.Init(ipV6, 'ip', position[0], position[1] - position[0])
            NamedEntities.append(ipV6Entity)
    
    def RecognizeUris(self, IndexName, AmbarFile, NamedEntities):
        uriRegex = re.compile('(^|\W)(((ftp[s]*)|(http[s]*)):(?://(?:((?:[a-z0-9-._~!$&*+,;=]|%[0-9A-F]{2})*)@)?((?:[a-z0-9-._~!$&*+,;=]|%[0-9A-F]{2})*)(?::(\d*))?(/(?:[a-z0-9-._~!$&*+,;=@/]|%[0-9A-F]{2})*)?|(/?(?:[a-z0-9-._~!$&*+,;=@]|%[0-9A-F]{2})+(?:[a-z0-9-._~!$&*+,;=@/]|%[0-9A-F]{2})*)?)(?:\?((?:[a-z0-9-._~!$&*+,;=/?@]|%[0-9A-F]{2})*))?(?:#((?:[a-z0-9-._~!$&*+,;=/?@]|%[0-9A-F]{2})*))?)($|\W)', re.M | re.I)
        for match in uriRegex.finditer(AmbarFile['content']['text']):
            uri = match.group(2)
            position = match.span(2)
            uriEntity = AmbarNamedEntity.Init(uri, 'uri', position[0], position[1] - position[0])
            NamedEntities.append(uriEntity)

    def CheckINN(self, Inn):
        if len(Inn) not in (10, 12):
            return False
    
        def InnCsum(inn):
            k = (3, 7, 2, 4, 10, 3, 5, 9, 4, 6, 8)
            pairs = zip(k[11-len(inn):], [int(x) for x in inn])
            return str(sum([k * v for k, v in pairs]) % 11 % 10)
    
        if len(Inn) == 10:
            return Inn[-1] == InnCsum(Inn[:-1])
        else:
            return Inn[-2:] == InnCsum(Inn[:-2]) + InnCsum(Inn[:-1])