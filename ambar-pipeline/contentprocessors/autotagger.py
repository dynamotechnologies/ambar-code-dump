from model import AmbarTaggingRule
from apiproxy import ApiProxy
from logger import AmbarLogger
from parsers.contenttypeanalyzer import ContentTypeAnalyzer
import re

class AutoTagger:
    def __init__(self, Logger, ApiProxy):
        self.logger = Logger
        self.apiProxy = ApiProxy
        self.AUTO_TAG_TYPE = 'auto'
        self.SOURCE_TAG_TYPE = 'source'
    
    def AutoTagAmbarFile(self, IndexName, AmbarFile, NamedEntities):
        self.SetOCRTag(IndexName, AmbarFile)
        self.SetSourceIdTag(IndexName, AmbarFile)
        self.SetArchiveTag(IndexName, AmbarFile)
        self.SetImageTag(IndexName, AmbarFile)

        self.ProcessNamedEntities(IndexName, AmbarFile, NamedEntities)

        for rule in self.GetTaggingRules():
            self.ProcessTaggingRule(rule, IndexName, AmbarFile)

    def ProcessNamedEntities(self, IndexName, AmbarFile, NamedEntities):
        tags = []
        for namedEntity in NamedEntities:
            if not namedEntity.type in tags:
                tags.append(namedEntity.type)
        for tag in tags:
            self.AddTagToAmbarFile(IndexName, AmbarFile['file_id'], AmbarFile['meta']['full_name'], self.AUTO_TAG_TYPE, tag)

    def ProcessTaggingRule(self, TaggingRuleToProcess, IndexName, AmbarFile):
        try:
            if TaggingRuleToProcess.field == 'content':
                match = re.search(TaggingRuleToProcess.regex, AmbarFile['content']['text'])
            elif TaggingRuleToProcess.field == 'path':
                match = re.search(TaggingRuleToProcess.regex, AmbarFile['meta']['full_name'])
            else:
                self.logger.LogMessage('error', 'error applying autotagging rule {0}, no such field known {1}'.format(TaggingRuleToProcess.name, TaggingRuleToProcess.field))
                return

            if match:
                for tag in TaggingRuleToProcess.tags:
                    self.AddTagToAmbarFile(IndexName, AmbarFile['file_id'], AmbarFile['meta']['full_name'], self.AUTO_TAG_TYPE, tag)
        except Exception as ex:
            self.logger.LogMessage('error', 'error applying autotagging rule {0} to {1} {2}'.format(TaggingRuleToProcess.name, AmbarFile['meta']['full_name'], str(ex)))

    def GetTaggingRules(self):
        taggingRules = []

        apiResp = self.apiProxy.GetTaggingRules()

        if not apiResp.Success: 
            self.logger.LogMessage('error', 'error retrieving autotagging rules {0}'.format(apiResp.message))
            return taggingRules
        
        if not (apiResp.Ok):
            self.logger.LogMessage('error', 'error retrieving autotagging rules, unexpected response code {0} {1}'.format(apiResp.code, apiResp.message))
            return taggingRules

        for ruleDict in apiResp.payload:
            taggingRules.append(AmbarTaggingRule.Init(ruleDict))

        return taggingRules
    
    def SetSourceIdTag(self, IndexName, AmbarFile):
        self.AddTagToAmbarFile(IndexName, AmbarFile['file_id'], AmbarFile['meta']['full_name'] ,self.SOURCE_TAG_TYPE, AmbarFile['meta']['source_id'])        

    def SetOCRTag(self, IndexName, AmbarFile):
        if AmbarFile['content']['ocr_performed']:
            self.AddTagToAmbarFile(IndexName, AmbarFile['file_id'], AmbarFile['meta']['full_name'], self.AUTO_TAG_TYPE, 'ocr')

    def SetArchiveTag(self, IndexName, AmbarFile):
        if ContentTypeAnalyzer.IsArchive(AmbarFile['meta']['full_name']):
            self.AddTagToAmbarFile(IndexName, AmbarFile['file_id'], AmbarFile['meta']['full_name'], self.AUTO_TAG_TYPE, 'archive')

    def SetImageTag(self, IndexName, AmbarFile):
        if ContentTypeAnalyzer.IsImageByContentType(AmbarFile['content']['type']):
            self.AddTagToAmbarFile(IndexName, AmbarFile['file_id'], AmbarFile['meta']['full_name'], self.AUTO_TAG_TYPE, 'image')

    def AddTagToAmbarFile(self, IndexName, FileId, FullName, TagType, Tag):
        apiResp = self.apiProxy.AddFileTag(IndexName, FileId, TagType, Tag)

        if not apiResp.Success: 
            self.logger.LogMessage('error', 'error adding {0} tag to file {1} {2}'.format(Tag, FullName, apiResp.message))
            return False
        
        if not (apiResp.Ok or apiResp.Created):
            self.logger.LogMessage('error', 'error adding {0} tag to file, unexpected response code {1} {2} {3}'.format(Tag, FullName, apiResp.code, apiResp.message))
            return False
        
        self.logger.LogMessage('verbose', '{0} tag added to {1}'.format(Tag, FullName))

