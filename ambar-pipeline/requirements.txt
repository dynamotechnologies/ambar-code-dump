pika==0.10.0
Cython==0.25.2
Pillow==4.0.0
pyocr==0.4.6
requests==2.12.4
six==1.10.0
chardet==2.3.0